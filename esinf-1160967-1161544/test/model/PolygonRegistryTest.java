/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test class for PolygonRegistry.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class PolygonRegistryTest {

    @Test
    public void ensureFillTreesFillsOnesTree() {
        String onesFileName = "poligonos_prefixo_unidades.txt";
        String tensFileName = "poligonos_prefixo_dezenas_teste.txt";
        String hundredsFileName = "poligonos_prefixo_centenas.txt";
        String[] filenames = new String[3];
        filenames[0] = onesFileName;
        filenames[1] = tensFileName;
        filenames[2] = hundredsFileName;

        PolygonRegistry pr = new PolygonRegistry(filenames);

        AVL<Polygon> expected = new AVL<>();
        expected.insert(new Polygon(1, "hena"));
        expected.insert(new Polygon(2, "di"));
        expected.insert(new Polygon(3, "tri"));
        expected.insert(new Polygon(4, "tetra"));
        expected.insert(new Polygon(5, "penta"));
        expected.insert(new Polygon(6, "hexa"));
        expected.insert(new Polygon(7, "hepta"));
        expected.insert(new Polygon(8, "octa"));
        expected.insert(new Polygon(9, "ennea"));

        AVL<Polygon> result = pr.getOnes();

        assertTrue(expected.equals(result));
    }

    @Test
    public void ensureFillTreesFillsTensTree() {
        String onesFileName = "poligonos_prefixo_unidades.txt";
        String tensFileName = "poligonos_prefixo_dezenas_teste.txt";
        String hundredsFileName = "poligonos_prefixo_centenas.txt";
        String[] filenames = new String[3];
        filenames[0] = onesFileName;
        filenames[1] = tensFileName;
        filenames[2] = hundredsFileName;

        PolygonRegistry pr = new PolygonRegistry(filenames);

        AVL<Polygon> expected = new AVL<>();
        expected.insert(new Polygon(10, "deca"));
        expected.insert(new Polygon(11, "hendeca"));
        expected.insert(new Polygon(12, "dodeca"));
        expected.insert(new Polygon(13, "triskaideca"));
        expected.insert(new Polygon(14, "tetrakaideca"));

        AVL<Polygon> result = pr.getTens();

        assertTrue(expected.equals(result));
    }

    @Test
    public void ensureFillTreesFillsHundredsTree() {
        String onesFileName = "poligonos_prefixo_unidades.txt";
        String tensFileName = "poligonos_prefixo_dezenas_teste.txt";
        String hundredsFileName = "poligonos_prefixo_centenas.txt";
        String[] filenames = new String[3];
        filenames[0] = onesFileName;
        filenames[1] = tensFileName;
        filenames[2] = hundredsFileName;

        PolygonRegistry pr = new PolygonRegistry(filenames);

        AVL<Polygon> expected = new AVL<>();
        expected.insert(new Polygon(100, "hecta"));
        expected.insert(new Polygon(200, "dihecta"));
        expected.insert(new Polygon(300, "trihecta"));
        expected.insert(new Polygon(400, "tetrahecta"));
        expected.insert(new Polygon(500, "pentahecta"));
        expected.insert(new Polygon(600, "hexahecta"));
        expected.insert(new Polygon(700, "heptahecta"));
        expected.insert(new Polygon(800, "octahecta"));
        expected.insert(new Polygon(900, "enneahecta"));

        AVL<Polygon> result = pr.getHundreds();

        assertTrue(expected.equals(result));
    }

    @Test
    public void ensurePolygonNameByNumberOfSidesIsWorking() {
        String fileName = "teste_lados_nome.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int nPolygons = 999;
            String line;
            String[] lineSplit;
            String expected;
            String result;
            for (int i = 1; i <= nPolygons; i++) {
                line = in.nextLine();
                lineSplit = line.split(";");
                expected = lineSplit[1];
                result = pr.polygonNameByNumberOfSides(Integer.parseInt(lineSplit[0]));
                assertTrue(expected.equalsIgnoreCase(result));
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensureAllPolygonsTreeIsWorking() {
        String fileName = "teste_lados_nome.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            AVL<Polygon> expected = new AVL<>();
            int nPolygons = 999;
            String line;
            String[] lineSplit;
            Polygon p;
            int numberOfSides;
            String polygonName;
            for (int i = 1; i <= nPolygons; i++) {
                line = in.nextLine();
                lineSplit = line.split(";");
                numberOfSides = Integer.parseInt(lineSplit[0]);
                polygonName = lineSplit[1];
                p = new Polygon(numberOfSides, polygonName);
                expected.insert(p);
            }
            AVL<Polygon> result = pr.allPolygonsTree();
            assertTrue(expected.equals(result));
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensurePolygonNumberOfSidesByNameIsWorking() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int nPolygons = 999;
            String line;
            String[] lineSplit;
            int expected;
            int result;
            for (int i = 1; i <= nPolygons; i++) {
                line = in.nextLine();
                lineSplit = line.split(";");
                expected = Integer.parseInt(lineSplit[1]);
                result = pr.polygonNumberOfSidesByName(lineSplit[0]);
                assertEquals(expected, result);
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensurePolygonNumberOfSidesByNameReturnMinusOne() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int expected = -1;
            int result = pr.polygonNumberOfSidesByName("triangle");
            assertEquals(expected, result);
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensurePolygonNamesGivenNSidesIntervalInvalidIntervalReturnsNull() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int min = 30;
            int max = 20;
            List<String> expected = null;
            Iterable<String> result = pr.polygonNamesGivenNSidesInterval(min, max);
            assertEquals(expected, result);
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensurePolygonNamesGivenNSidesIntervalInvalidMinReturnsNull() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int min = -2;
            int max = 20;
            List<String> expected = null;
            Iterable<String> result = pr.polygonNamesGivenNSidesInterval(min, max);
            assertEquals(expected, result);
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensurePolygonNamesGivenNSidesIntervalInvalidMaxReturnsNull() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int min = 20;
            int max = 1001;
            List<String> expected = null;
            Iterable<String> result = pr.polygonNamesGivenNSidesInterval(min, max);
            assertEquals(expected, result);
        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensurePolygonNamesGivenNSidesIntervalIsWorking() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            int min = 5;
            int max = 9;
            String s1 = "enneagon";
            String s2 = "octagon";
            String s3 = "heptagon";
            String s4 = "hexagon";
            String s5 = "pentagon";
            List<String> expected = new ArrayList<>();
            Iterable<String> tree = pr.polygonNamesGivenNSidesInterval(min, max);
            List<String> result = new ArrayList<>();
            for (String s : tree) {
                result.add(s);
            }
            assertEquals(expected, result);

        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    @Test
    public void ensureLowestCommonAncestorIsWorking() {
        String fileName = "teste_nome_lados.txt";

        try (Scanner in = new Scanner(new File(fileName))) {
            String onesFileName = "poligonos_prefixo_unidades.txt";
            String tensFileName = "poligonos_prefixo_dezenas.txt";
            String hundredsFileName = "poligonos_prefixo_centenas.txt";
            String[] filenames = new String[3];
            filenames[0] = onesFileName;
            filenames[1] = tensFileName;
            filenames[2] = hundredsFileName;
            PolygonRegistry pr = new PolygonRegistry(filenames);
            String p1 = "henagon";
            String p2 = "digon";
            String p3 = "trigon";
            String p4 = "tetragon";
            String p5 = "pentagon";
            String p6 = "hexagon";
            String p7 = "heptagon";
            String expected;
            String result;
            
            expected = p4;
            result = pr.lowestCommonAncestor(p2, p6);
            assertTrue(expected.equalsIgnoreCase(result));
            
            expected = p4;
            result = pr.lowestCommonAncestor(p3, p5);
            assertTrue(expected.equalsIgnoreCase(result));
            
            expected = p2;
            result = pr.lowestCommonAncestor(p1, p2);
            assertTrue(expected.equalsIgnoreCase(result));
            
            expected = p6;
            result = pr.lowestCommonAncestor(p6, p7);
            assertTrue(expected.equalsIgnoreCase(result));
            

        } catch (FileNotFoundException fnfe) {
            System.out.println("Ficheiro não encontrado!");
        }
    }

    

}
