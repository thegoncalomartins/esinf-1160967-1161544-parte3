/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import model.BST.Node;

/**
 *
 * @author gmartins
 */
public class PolygonRegistry {

    /**
     * The AVL Tree to save the "ones" prefixes.
     */
    private AVL<Polygon> ones;

    /**
     * The AVL Tree to save the "tens" prefixes.
     */
    private AVL<Polygon> tens;

    /**
     * The AVL Tree to save the "hundreds" prefixes.
     */
    private AVL<Polygon> hundreds;

    /**
     * The AVL Tree which contains all polygons from 1 to 999 sides.
     */
    private AVL<Polygon> all = new AVL<>();

    /**
     * The minimum sides.
     */
    private static final int MIN = 1;

    /**
     * The maximum sides.
     */
    private static final int MAX = 999;

    /**
     * Creates a new PolygonRegistry instance, initiating the trees.
     */
    public PolygonRegistry(String[] filenames) {
        ones = new AVL<>();
        tens = new AVL<>();
        hundreds = new AVL<>();
        fillTrees(filenames);
    }

    /**
     * @return the ones
     */
    public AVL<Polygon> getOnes() {
        return ones;
    }

    /**
     * @return the tens
     */
    public AVL<Polygon> getTens() {
        return tens;
    }

    /**
     * @return the hundreds
     */
    public AVL<Polygon> getHundreds() {
        return hundreds;
    }

    /**
     * @return the all
     */
    public AVL<Polygon> getAll() {
        return all;
    }

    /**
     * a) Fills the trees with the prefixes of the number of sides of the polygon.
     *
     * @param filenames array of strings with the files names
     */
    public void fillTrees(String[] filenames) {

        fillTree(filenames[0], ones);
        fillTree(filenames[1], tens);
        fillTree(filenames[2], hundreds);
    }

    /**
     * Reads the file with the filename received as parameter and inserts the polygons into the respective AVL's.
     *
     * @param filename the filename
     * @param tree the tree to be filled with the polygons provided by the text file.
     */
    private void fillTree(String filename, AVL<Polygon> tree) {

        try (Scanner in = new Scanner(new File(filename))) {
            int numberOfSides;
            String line;
            String[] aux;
            String prefix;
            while (in.hasNext()) {
                line = in.nextLine();
                aux = line.split(";");
                numberOfSides = Integer.parseInt(aux[0]);
                prefix = aux[1];
                Polygon pol = new Polygon(numberOfSides, prefix);
                tree.insert(pol);

            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + filename + "was not found!");
        } catch (NumberFormatException nfe) {
            System.out.println("Error occured while reading integer number!");
        }
    }

    /**
     * Returns the Polygon if it exists in the tree.
     *
     * @param element the element to find
     * @return the Node that contains the Element, or null otherwise
     */
    private Node<Polygon> find(AVL<Polygon> tree, Polygon element) {
        if (element == null) {
            return null;
        }
        return tree.find(element, tree.root);
    }

    private String findTens(int numberOfSides) {
        int midInterval = 30;
        int onesAlgarism;
        int tensNumber;
        Polygon p;

        StringBuilder sb = new StringBuilder();

        if (numberOfSides < midInterval) {
            p = find(getTens(), new Polygon(numberOfSides, "")).getElement();
            return p.getPrefix();
        } else {
            onesAlgarism = numberOfSides % 10;
            tensNumber = (numberOfSides / 10) * 10;
            p = find(getTens(), new Polygon(tensNumber, "")).getElement();
            sb.append(p.getPrefix());

            if (onesAlgarism != 0) {
                p = find(getOnes(), new Polygon(onesAlgarism, "")).getElement();
                sb.append(p.getPrefix());
            }
        }

        return sb.toString();
    }

    private String findHundreds(int numberOfSides) {
        int tensNumber;
        int maxOnes = 10;
        Polygon p;

        StringBuilder sb = new StringBuilder();

        int hundredsNumber = (numberOfSides / 100) * 100;
        p = find(getHundreds(), new Polygon(hundredsNumber, "")).getElement();
        sb.append(p.getPrefix());

        tensNumber = numberOfSides % 100;

        if (tensNumber != 0) {
            if (tensNumber < maxOnes) {
                p = find(getOnes(), new Polygon(tensNumber, "")).getElement();
                sb.append(p.getPrefix());
            } else {
                sb.append(findTens(tensNumber));
            }
        }

        return sb.toString();
    }

    /**
     * b) Returns the name of the polygon with the number of sides received by parameter.
     *
     * @param numberOfSides the number of sides of the polygon
     * @return the name of the polygon or null if the numberOfSides is not in the interval [1, 999]
     */
    public String polygonNameByNumberOfSides(int numberOfSides) {

        if (numberOfSides < MIN || numberOfSides > MAX) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        int maxOnes = 10;
        int maxTens = 99;
        Polygon p;

        if (numberOfSides < maxOnes) {
            p = find(getOnes(), new Polygon(numberOfSides, "")).getElement();
            sb.append(p.getPrefix());
        } else if (numberOfSides <= maxTens) {
            // it's between 10 and 99
            sb.append(findTens(numberOfSides));
        } else {
            // it's between 100 and 999
            sb.append(findHundreds(numberOfSides));
        }

        sb.append("gon"); // end the name
        return sb.toString();
    }

    /**
     * c) Returns a tree with all the polygons from 1 to 999 sides.
     *
     * @return a tree with all the polygons from 1 to 999 sides
     */
    public AVL<Polygon> allPolygonsTree() {
        if (!all.isEmpty()) {
            return all;
        }

        String polygonName;

        for (int i = MIN; i <= MAX; i++) {
            polygonName = polygonNameByNumberOfSides(i);
            all.insert(new Polygon(i, polygonName));
        }

        return all;

    }

    /**
     * d) Returns the number of sides of the polygon with the name received by parameter.
     *
     * @param name the name of the polygon
     * @return the number of sides of the polygon, -1 if didn't find the number of sides with the received name
     */
    public int polygonNumberOfSidesByName(String name) {
        String name1;
        for (int i = MIN; i <= MAX; i++) {
            name1 = polygonNameByNumberOfSides(i);

            if (name1.equalsIgnoreCase(name)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * e) Returns a list of polygon names with the number of sides in the interval received by parameter.
     *
     * @param min the minimum number of sides
     * @param max the maximum number of sides
     * @return a list of polygon names
     */
    public Iterable<String> polygonNamesGivenNSidesInterval(int min, int max) {

        if (min > max || min < MIN || max > MAX) {
            return null;
        }

        List<String> list = new LinkedList<>();
        String name;
        for (int i = max; i <= min; i--) {
            name = polygonNameByNumberOfSides(i);
            list.add(name);
        }

        return list;
    }

    /**
     * f) Returns the name of the lowest common ancestor of the two polygons received by parameter.
     *
     * @param p1 the first polygon
     * @param p2 the seconds polygon
     * @return the name of the lowest common ancestor
     */
    public String lowestCommonAncestor(String p1, String p2) {
        allPolygonsTree();
        Node<Polygon> r = all.root;
        int number1 = polygonNumberOfSidesByName(p1);
        int number2 = polygonNumberOfSidesByName(p2);
        Node<Polygon> n1 = find(all, new Polygon(number1, ""));
        Node<Polygon> n2 = find(all, new Polygon(number2, ""));

        return lowestCommonAncestor(r, n1, n2);
    }

    private String lowestCommonAncestor(Node<Polygon> root, Node<Polygon> p1, Node<Polygon> p2) {
        if (root.getElement().compareTo(p1.getElement()) > 0 && root.getElement().compareTo(p2.getElement()) < 0) {
            return root.getElement().getPrefix();
        } else if (root.getElement().compareTo(p1.getElement()) > 0 && root.getElement().compareTo(p2.getElement()) > 0) {
            return lowestCommonAncestor(root.getLeft(), p1, p2);
        } else if (root.getElement().compareTo(p1.getElement()) < 0 && root.getElement().compareTo(p2.getElement()) < 0) {
            return lowestCommonAncestor(root.getRight(), p1, p2);
        }

        return root.getElement().getPrefix();
    }

}
