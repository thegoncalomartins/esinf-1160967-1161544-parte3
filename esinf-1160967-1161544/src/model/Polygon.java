/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author gmartins
 */
public class Polygon implements Comparable<Polygon> {
    
    /**
     * The number of the sides of the polygon.
     */
    private int nSides;
    /**
     * The prefix of the name of the polygon.
     */
    private String prefix;
    
    /**
     * Creates a new polygon with the number of sides passed by parameter.
     * @param nSides the number of sides.
     * @param prefix the prefix of the name of the polygon
     */
    public Polygon(int nSides, String prefix) {
        this.nSides = nSides;
        this.prefix = prefix;
    }

    /**
     * @return the nSides
     */
    public int getnSides() {
        return nSides;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    @Override
    public int compareTo(Polygon other) {
        return new Integer(nSides).compareTo(other.getnSides());
    }
}
